<?php

namespace App\Http\Livewire\Customer\Auth;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Logout extends Component
{
    /**
     * 
     */
    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect()->route('customer.auth.login');
    }

    public function render()
    {
        return view('livewire.customer.auth.logout');
    }
}
